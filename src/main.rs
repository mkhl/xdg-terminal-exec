use std::{
    collections::HashSet, env, ffi::OsString, fs, iter::once, os::unix::process::CommandExt,
    path::PathBuf, process::Command,
};

use eyre::{eyre, OptionExt, Result};
use freedesktop_entry_parser::Entry;

const XDG_TERMINALS_LIST: &str = "xdg-terminals.list";
const DESKTOP_ENTRY: &str = "Desktop Entry";

type Config = (Entry, Option<String>);

struct System {
    dirs: xdg::BaseDirectories,
    desktops: Vec<String>,
}

fn desktops() -> Vec<String> {
    env::var("XDG_CURRENT_DESKTOP")
        .unwrap_or_default()
        // .to_ascii_lowercase()
        .split(':')
        .filter(|s| !s.is_empty())
        .map(Into::into)
        .collect()
}

impl System {
    fn new() -> Result<Self> {
        let dirs = xdg::BaseDirectories::new()?;
        let desktops = desktops();
        Ok(System { dirs, desktops })
    }

    fn config_files(&self) -> impl Iterator<Item = PathBuf> + '_ {
        self.desktops
            .iter()
            .map(|desktop| format!("{}-{}", desktop, XDG_TERMINALS_LIST))
            .chain(once(XDG_TERMINALS_LIST.into()))
            .flat_map(|name| self.dirs.find_config_file(name))
    }

    fn is_desktop_active(&self, desktop: &str) -> bool {
        self.desktops.iter().any(|it| *it == desktop)
    }

    fn is_entry_usable(&self, entry: &Entry) -> bool {
        let section = entry.section(DESKTOP_ENTRY);
        if section.attr("Hidden") == Some("true") {
            return false;
        }
        if section.attr("NotShowIn").is_some_and(|list| {
            list.split_terminator(';')
                .any(|it| self.is_desktop_active(it))
        }) {
            return false;
        }
        if section.attr("OnlyShowIn").is_some_and(|list| {
            !list
                .split_terminator(';')
                .any(|it| self.is_desktop_active(it))
        }) {
            return false;
        }
        if section
            .attr("TryExec")
            .is_some_and(|it| which::which(it).is_err())
        {
            return false;
        }
        section.attr("Categories").is_some_and(|list| {
            list.split_terminator(';')
                .any(|it| it == "TerminalEmulator")
        })
    }

    fn find_configured_terminals(&self) -> impl Iterator<Item = Config> + '_ {
        self.config_files()
            .flat_map(fs::read_to_string)
            .flat_map(|text| {
                text.lines()
                    .map(|line| line.trim())
                    .filter(|line| {
                        line.starts_with(|c: char| c == '_' || c.is_ascii_alphanumeric())
                    })
                    .map(Into::into)
                    .collect::<Vec<_>>()
            })
            .flat_map(|line| self.get_configured_terminal(line))
            .filter(|(entry, _)| self.is_entry_usable(entry))
    }

    fn get_configured_terminal(&self, line: String) -> Option<Config> {
        let (name, action) = match line.split_once(':') {
            None => (line, None),
            Some((name, action)) => (name.into(), Some(action.into())),
        };
        let path = PathBuf::from("applications").join(name);
        let path = self.dirs.find_data_file(path)?;
        let entry = Entry::parse_file(path).ok()?;
        Some((entry, action))
    }

    fn find_present_terminals(&self) -> impl Iterator<Item = Entry> + '_ {
        let mut seen: HashSet<OsString> = HashSet::new();
        self.dirs
            .find_data_files("applications")
            .rev() // try more specific directories first
            .flat_map(fs::read_dir)
            .flatten()
            .flatten()
            .map(|dir| dir.path())
            .filter(move |path| {
                path.file_name()
                    .is_some_and(|name| seen.insert(name.into()))
            }) // reject shadowed applications
            .flat_map(Entry::parse_file)
            .filter(|entry| self.is_entry_usable(entry))
    }
}

fn run_entry(entry: &Entry, action: &Option<String>, user_args: &[String]) -> Result<()> {
    let name = match action {
        None => DESKTOP_ENTRY.into(),
        Some(action) => format!("Desktop Action {}", action),
    };
    if !entry.has_section(&name) {
        return Err(eyre!("required section `{}` not found", name));
    }

    let section = entry.section(&name);
    let exec = section
        .attr("Exec")
        .ok_or_eyre("required attribute `Exec` not found")?;
    let exec = &shlex::split(exec).ok_or_eyre("failed to parse `Exec` attribute")?;
    let (term, term_args) = exec
        .split_first()
        .ok_or_eyre("failed to parse `Exec` attribute")?;

    let section = entry.section(DESKTOP_ENTRY);
    let exec_arg = section
        .attr("X-ExecArg")
        .or(section.attr("ExecArg"))
        .unwrap_or("-e");

    run(term, term_args, exec_arg, user_args)
}

fn run(term: &str, term_args: &[String], args_sep: &str, user_args: &[String]) -> Result<()> {
    let mut cmd = Command::new(term);
    cmd.args(term_args);
    if !user_args.is_empty() {
        if !args_sep.is_empty() {
            cmd.arg(args_sep);
        }
        cmd.args(user_args);
    }
    Err(eyre!(cmd.exec()))
}

fn main() -> Result<()> {
    let args = env::args().skip(1).collect::<Vec<_>>();
    let sys = System::new()?;
    for (terminal, action) in sys.find_configured_terminals() {
        run_entry(&terminal, &action, args.as_slice())?;
    }
    for terminal in sys.find_present_terminals() {
        run_entry(&terminal, &None, args.as_slice())?;
    }
    run("xterm", &[], "-e", args.as_slice())
}
